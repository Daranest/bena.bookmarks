<?
use \Bitrix\Main\Loader;
use \Bitrix\Main\Type;
use \Bitrix\Main\Application;
use \Bena\Bookmarks\DataTable;
use \Bena\Bookmarks\Helper;
if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

class AddMark extends CBitrixComponent
{

    /** @var  \Bitrix\Main\HttpRequest */
    protected $request;
    public $bookmark_url = null;

    private function _checkModules()
    {
        if (!Loader::includeModule('bena.bookmarks')) {
            throw new \Exception('Не загружены модули необходимые для работы модуля');
        }
        return true;
    }
    public static function sendJsonAnswer($result)
    {
        global $APPLICATION;

        $APPLICATION->RestartBuffer();
        header('Content-Type: application/json');

        echo \Bitrix\Main\Web\Json::encode($result);

        CMain::FinalActions();
        die();
    }
    public function addUrl(){
        global $DB;
        if ($this->bookmark_url === null) {
            $result = ["status" => "error", "data" => ["message"=> "Не корректный url"]];
            self::sendJsonAnswer($result);
        }
        $curl_data = Helper::getBookmarkCurl($this->bookmark_url);
        $favicon = Helper::getFavicon($curl_data['content'], $this->bookmark_url);
        $keywords = Helper::getKeywords($curl_data['content']);
        $description = Helper::getDescription($curl_data['content']);
        $title = Helper::getTitle($curl_data['content'], $this->bookmark_url);
        $result_add = false;

        try{
            $result_add = DataTable::add(
                [
                    "URL" =>  $this->bookmark_url,
                    "FAVICON"=> $favicon,
                    "TITLE" => $title,
                    "ADD_DATE" =>  new Type\DateTime(),
                    "META_DESCRIPTION" => $description,
                    "META_KEYWORDS" => $keywords,
                ]
            );
        }catch (\Bitrix\Main\DB\Exception $e){
            if(strpos($e->getMessage(), 'Duplicate entry') !== false){
                $message = "url уже был добавлен";
            } else {
                $message = $e->getMessage();
            }
            $result = ["status" => "error", "data" => ["message"=> $message]];
            self::sendJsonAnswer($result);
        }

        $result = [
            "status" => "success",
            "data" => [
                "id" =>$result_add->getId(),
                "add_url" => $this->arParams['SEF_FOLDER'].$result_add->getId().'/',
                "message" => "Закладка успешно добавлена"
            ]
        ];
        self::sendJsonAnswer($result);

    }
    public function executeComponent()
    {
        $this->_checkModules();

        if ($this->request->isPost())
        {
            $this->bookmark_url =  $this->request->get("url");
            $this->addUrl();
        } else {
            $this->includeComponentTemplate();
        }
    }
}
