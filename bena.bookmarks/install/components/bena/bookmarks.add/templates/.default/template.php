﻿<?

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Application;

/**
 * @var array $arParams
 * @var array $arResult
 * @var CMain $APPLICATION
 * @var CUser $USER
 * @var $component
 * @var string $templateFolder
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
$this->addExternalJs( '/bitrix/js/bena.bookmarks/jquery-3.4.1.min.js');
$this->addExternalJs( '/bitrix/js/bena.bookmarks/bootstrap.bundle.min.js');
$this->addExternalJs( '/bitrix/js/bena.bookmarks/bena.bookmarks.js');
$this->addExternalCss('/bitrix/css/bena.bookmarks/bena.bookmarks.css');
?>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <h1>Введите URL закладки</h1>
        </div>
        <div class="col-md-6">
            <a class="list-link" href="<?=$arParams['SEF_FOLDER']?>list/">Перейти к списку закладок</a>
        </div>
    </div>
    <div class="row">
        <form id="BookmarkAdd" class="form-horizontal">

            <div class="input-group">
                <input type="text" placeholder="http://"
                       id="url" name="url" class="form-control box-placeholder"
                       data-toggle="popover"
                       data-placement="bottom"
                       title="Ошибка добавления"
                       data-content="">
                <span class="input-group-append">
                    <button class="btn btn-info p-0 border-0 border px-4 font-weight-bold" type="submit">
                     Добавить
                     </button>
                </span>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="input-group mt-3">
                        <div class="input-group-prepend">
                            <div class="input-group-text">
                                Защитить закладку паролем
                            </div>
                            <div class="input-group-text">
                                <input type="checkbox" id="usePass" aria-label="Защитить закладку паролем">
                            </div>
                        </div>
                        <input type="password" class="form-control" id="password" name="password" placeholder="Введите пароль для защищенной закладки">
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>

