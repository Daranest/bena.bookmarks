<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc;
$arComponentDescription = [
    "NAME" => Loc::getMessage("BN_DETAIL_NAME"),
    "DESCRIPTION" => Loc::getMessage("BN_DETAIL_DESCRIPTION"),
    "COMPLEX" => "N",
    "PATH" => [
        "ID" => Loc::getMessage("BN_DETAIL_PATH_ID"),
        "NAME" => Loc::getMessage("BN_DETAIL_PATH_NAME"),
    ],
];
?>