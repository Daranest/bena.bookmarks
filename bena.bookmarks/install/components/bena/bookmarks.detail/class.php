<?
use \Bitrix\Main\Loader;
use \Bitrix\Main\Application;
use \Bena\Bookmarks;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

class ViewBookmark extends CBitrixComponent
{

    /** @var  \Bitrix\Main\HttpRequest */
    protected $request;
    public $bookmark_id = null;

    private function _checkModules()
    {
        if (!Loader::includeModule('bena.bookmarks')) {
            throw new \Exception('Не загружены модули необходимые для работы модуля');
        }
        return true;
    }
    public function getBookmark(){
        $params = [
            'filter' => ['ID'=>  $this->bookmark_id],
            'limit'=> 1
        ];
        $dbRes = Bookmarks\DataTable::getList($params);
        $this->arResult = $dbRes->fetch();
    }
    public function executeComponent()
    {
        $this->_checkModules();
        if($this->arParams['BOOKMARK_ID']){
            $this->bookmark_id = $this->arParams['BOOKMARK_ID'];
        }
        if($this->request->get("id")){
            $this->bookmark_id =  $this->request->get("id");
        }
        $this->getBookmark();
        $this->includeComponentTemplate();

    }
}
