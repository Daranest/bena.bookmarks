﻿<?

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Page\Asset;

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/**
 * @var array $arParams
 * @var array $arResult
 * @var CMain $APPLICATION
 * @var CUser $USER
 * @var $component
 * @var string $templateFolder
 */
CJSCore::Init(array("jquery"));
$this->addExternalJs( '/bitrix/js/bena.bookmarks/bena.bookmarks.js');
$this->addExternalCss('/bitrix/css/bena.bookmarks/bena.bookmarks.css');
?>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <h1>Закладка добавлена</h1>
        </div>
        <div class="col-md-3">
            <a class="list-link" href="<?=$arParams['SEF_FOLDER']?>">Добавить еще url</a>
        </div>
        <div class="col-md-3">
            <a class="list-link" href="<?=$arParams['SEF_FOLDER']?>list/">Перейти к списку закладок</a>
        </div>
    </div>
    <div class="row">
        <table class="table bookmarks-table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Favicon</th>
                <th scope="col">Заголовок страницы</th>
                <th scope="col">Url страницы</th>
                <th scope="col">Дата добавления</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row"><?=$arResult['ID']?></th>
                <td><img class="favico" src="<?=$arResult['FAVICON']?>"></td>
                <td><?=$arResult['TITLE']?></td>
                <td><a href="<?=$arResult['URL']?>" target="_blank"><?=$arResult['URL']?></a></td>
                <td><?=$arResult['ADD_DATE']?></td>
            </tr>
            </tbody>
        </table>
       </div>
</div>
