<?
use \Bitrix\Main\Loader;
use \Bitrix\Main\Application;
use \Bena\Bookmarks;

if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

class ListBookmark extends CBitrixComponent
{

    /** @var  \Bitrix\Main\HttpRequest */
    public $navigation;
    protected $request;

    private function _checkModules()
    {
        if (!Loader::includeModule('bena.bookmarks')) {
            throw new \Exception('Не загружены модули необходимые для работы модуля');
        }
        return true;
    }
    public function getBookmarks(){
        $this->navigation = new \Bitrix\Main\UI\PageNavigation("nav-url");
        $this->navigation->allowAllRecords(true)
            ->setPageSize(7)
            ->initFromUri();
        $params = [
            'order' => [$this->arParams['SORT_BY'] => $this->arParams['ORDER_BY']],
            'count_total' => true,
            'offset' => $this->navigation->getOffset(),
            'limit' => $this->navigation->getLimit(),
        ];
        $dbRes = Bookmarks\DataTable::getList($params);
        $this->navigation->setRecordCount($dbRes->getCount());

        while($arRes = $dbRes->fetch()){
            $this->arResult['ITEMS'][] = $arRes;
        }
        $this->arResult['nav_obj'] = $this->navigation;

    }
    public function executeComponent()
    {
        $this->_checkModules();
        $this->getBookmarks();
        $this->includeComponentTemplate();

    }
}
