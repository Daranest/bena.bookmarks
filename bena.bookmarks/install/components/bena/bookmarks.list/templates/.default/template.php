﻿<?

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/**
 * @var array $arParams
 * @var array $arResult
 * @var CMain $APPLICATION
 * @var CUser $USER
 * @var ListBookmark $component
 * @var string $templateFolder
 */
CJSCore::Init(array("jquery"));
$this->addExternalJs( '/bitrix/js/bena.bookmarks/bena.bookmarks.js');
$this->addExternalCss('/bitrix/css/bena.bookmarks/bena.bookmarks.css');
?>
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <h1>Список добавленых URL</h1>
        </div>
        <div class="col-md-6">
            <a class="list-link" href="<?=$arParams['SEF_FOLDER']?>">Добавить еще url</a>
        </div>
    </div>
    <div class="row">
        <table class="table bookmarks-table">
            <thead>
            <tr>
                <th scope="col">#</th>
                <th scope="col">Favicon</th>
                <th scope="col">Заголовок страницы</th>
                <th scope="col">Url страницы</th>
                <th scope="col">Дата добавления</th>
            </tr>
            </thead>
            <tbody>
            <?foreach ($arResult['ITEMS'] as $bookmark) {?>
            <tr>
                <th scope="row"><?=$bookmark['ID']?></th>
                <td><img class="favico" src="<?=$bookmark['FAVICON']?>"></td>
                <td><?=$bookmark['TITLE']?></td>
                <td><a href="<?=$bookmark['URL']?>" target="_blank"><?=$bookmark['URL']?></a></td>
                <td><?=$bookmark['ADD_DATE']?></td>
            </tr>
            <?}?>
            </tbody>
        </table>
        <div class="row">
            <div class="col-md-12">
                <?
                $APPLICATION->IncludeComponent(
                    "bitrix:main.pagenavigation",
                    "",
                    array(
                        "NAV_OBJECT" =>  $arResult['nav_obj'],
                        "SEF_MODE" => "N",
                    ),
                    false
                );
                ?>
            </div>
        </div>
       </div>
</div>
