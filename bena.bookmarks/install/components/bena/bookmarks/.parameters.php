<?php
$arComponentParameters = array(
    "GROUPS" => array(),
    "PARAMETERS" => array(
        "VARIABLE_ALIASES" => array(),
        "SEF_MODE" => Array(
            "add" => array(
                "NAME" => "Страница добавить закладку",
                "DEFAULT" => "",
                "VARIABLES" => array(),
            ),
            "list" => array(
                "NAME" => "Страница список всех закладок",
                "DEFAULT" => "list/",
                "VARIABLES" => array(),
            ),
            "detail" => array(
                "NAME" => "Детальный просмотр закладки",
                "DEFAULT" => "#BOOKMARK_ID#/",
                "VARIABLES" => array("BOOKMARK_ID"),
            )
        ),
        "SORT_BY" => array(
            "NAME" => "Поле для сортировки",
            "TYPE" => "STRING",
            "DEFAULT" => "DATE_ADD",
        ),
        "ORDER_BY" => array(
            "NAME" => "Направление для сортировки",
            "TYPE" => "STRING",
            "DEFAULT" => "DESC",
        )
    ),
);
