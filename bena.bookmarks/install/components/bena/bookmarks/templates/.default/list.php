<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @var CBitrixComponent $component */
$APPLICATION->IncludeComponent("bena:bookmarks.list", ".default",
    [
        "SORT_BY"=> $arParams["SORT_BY"],
        "ORDER_BY"=> $arParams["ORDER_BY"],
        "SEF_FOLDER" => $arParams["SEF_FOLDER"],
    ]
    , $component);