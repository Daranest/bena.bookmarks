<?

IncludeModuleLangFile(__FILE__);

use \Bitrix\Main\ModuleManager;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Application;
use Bitrix\Main\IO\Directory;
use Bitrix\Main\Config\Option;

Class bena_bookmarks extends CModule
{
    var $MODULE_ID = "bena.bookmarks";
    var $MODULE_VERSION;
    var $MODULE_VERSION_DATE;
    var $MODULE_NAME;
    var $MODULE_DESCRIPTION;
    var $errors;

    function __construct()
    {
        if (file_exists(__DIR__ . "/version.php")) {
            $arModuleVersion = array();
            include(__DIR__ . "/version.php");

            $this->MODULE_ID = str_replace("_", ".", get_class($this));
            $this->MODULE_VERSION = $arModuleVersion["VERSION"];
            $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
            $this->MODULE_NAME = Loc::getMessage("BN_BK_INSTALL_NAME");
            $this->MODULE_DESCRIPTION = Loc::getMessage("BN_BK_INSTALL_DESCRIPTION");
            $this->PARTNER_NAME = Loc::getMessage("BN_BK_PARTNER_NAME");
            $this->PARTNER_URI = Loc::getMessage("BN_BK_PARTNER_URI");
        }
        return false;
    }

    function DoInstall()
    {
        $this->InstallDB();
        $this->InstallEvents();
        $this->InstallFiles();
        RegisterModule("bena.bookmarks");
        return true;
    }

    function DoUninstall()
    {
        $this->UnInstallDB();
        $this->UnInstallEvents();
        $this->UnInstallFiles();
        UnRegisterModule("bena.bookmarks");
        return true;
    }

    function InstallDB()
    {
        global $DB;
        $this->errors = false;
        $this->errors = $DB->RunSQLBatch( __DIR__ ."/db/mysql/install.sql");
        if (!$this->errors) {
            return true;
        } else {
            global $APPLICATION;
            $APPLICATION->ThrowException(implode('', $this->errors));
            return false;
        }

    }

    function UnInstallDB()
    {
        global $DB;
        $this->errors = false;
        $this->errors = $DB->RunSQLBatch( __DIR__ ."/db/mysql/unistall.sql");
        if (!$this->errors) {
            return true;
        } else {
            global $APPLICATION;
            $APPLICATION->ThrowException(implode('', $this->errors));
            return false;
        }

    }

    function InstallEvents()
    {
        return true;
    }

    function UnInstallEvents()
    {
        return true;
    }

    function InstallFiles()
    {
        $res = CopyDirFiles(
            __DIR__ . '/css/',
            Application::getDocumentRoot() . '/bitrix/css/' . $this->MODULE_ID . '/',
            true,
            true
        );

        $res = CopyDirFiles(
            __DIR__ . '/js/',
            Application::getDocumentRoot() . '/bitrix/js/' . $this->MODULE_ID . '/',
            true,
            true
        );
        $res = CopyDirFiles(
            __DIR__ . '/components/bena',
            Application::getDocumentRoot() . '/bitrix/components/bena/',
            true,
            true
        );

    }

    function UnInstallFiles()
    {
        Directory::deleteDirectory(
            Application::getDocumentRoot() . '/bitrix/js/' . $this->MODULE_ID
        );
        Directory::deleteDirectory(
            Application::getDocumentRoot() . '/bitrix/css/' . $this->MODULE_ID
        );
        Directory::deleteDirectory(
            Application::getDocumentRoot() . '/bitrix/components/bena'
        );
        Option::delete($this->MODULE_ID);
    }

}
?>