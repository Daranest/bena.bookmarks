$(document).on('submit', '#BookmarkAdd', function (e) {
    e.preventDefault();
    var data =  $(this).serialize();
    var urlInp = $('#url');
    $(urlInp).popover({delay: 1});
    $.ajax({
        type: "POST",
        data: data,
        dataType: "json",
        success: function (response) {
            if(response.status != 'success'){
                $(urlInp).attr('data-content', response.data.message);
                $(urlInp).popover('show');
                setTimeout(function(){
                    $(urlInp).popover('hide');
                }, 1500)
            } else {
                window.location.replace(response.data.add_url)
            }
        }
    });
});
$(document).on('change', '#usePass', function () {
    var passInp = $('#password');
    if($(this).is(':checked')){
        passInp.removeAttr('disabled');
    } else {
        passInp.attr('disabled', 'disabled');
    }
});