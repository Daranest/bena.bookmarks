<?php
$MESS["bookmarks_entity_field_url"] = "URL страницы";
$MESS["bookmarks_entity_field_title"] = "Заголовок страницы";
$MESS["bookmarks_entity_field_date"] = "Дата добавления";
$MESS["bookmarks_entity_field_favicon"] = "Ссылка на favicon";
$MESS["bookmarks_entity_field_meta_description"] = "META Description";
$MESS["bookmarks_entity_field_meta_keywords"] = "META Keywords";
$MESS["bookmarks_entity_field_password"] = "Пароль для закладки";