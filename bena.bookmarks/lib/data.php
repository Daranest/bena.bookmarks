<?php
namespace Bena\Bookmarks;

use Bitrix\Main\Entity;
use Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);


class DataTable extends Entity\DataManager
{
    /**
     * Returns DB table name for entity
     *
     * @return string
     */
    public static function getTableName()
    {
        return 'b_bena_bookmarks';
    }


    /**
     * Returns entity map definition
     *
     * @return array
     */
    public static function getMap()
    {
        return array(
            'ID' => array(
                'data_type' => 'integer',
                'primary' => true,
                'autocomplete' => true
            ),
            'URL' => array(
                'data_type' => 'string',
                'title'     => Loc::getMessage('bookmarks_entity_field_url'),
                'required'  => true
            ),
            'TITLE' => array(
                'data_type' => 'string',
                'title'     => Loc::getMessage('bookmarks_entity_field_title'),
                'required'  => true
            ),
            'ADD_DATE' => array(
                'data_type' => 'datetime',
                'title'     => Loc::getMessage('bookmarks_entity_field_date'),
                'required'  => true
            ),
            'FAVICON' => array(
                'data_type' => 'string',
                'title'     => Loc::getMessage('bookmarks_entity_field_favicon'),
            ),
            'META_DESCRIPTION' => array(
                'data_type' => 'string',
                'title'     => Loc::getMessage('bookmarks_entity_field_meta_description'),
            ),
            'META_KEYWORDS' => array(
                'data_type' => 'string',
                'title'     => Loc::getMessage('bookmarks_entity_field_meta_keywords'),
            ),
            'PASSWORD' => array(
                'data_type' => 'string',
                'title'     => Loc::getMessage('bookmarks_entity_field_password'),
            ),
        );
    }
}