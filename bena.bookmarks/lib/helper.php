<?php

namespace Bena\Bookmarks;

class Helper
{
    public static $base_url_regex = '/^.+?[^\/:](?=[?\/]|$)/m';
    public static function getBookmarkCurl($url, $options = false){
        if(!$options) {
            $options = array(
                CURLOPT_CUSTOMREQUEST  =>"GET",        //set request type post or get
                CURLOPT_POST           =>false,        //set to GET
                CURLOPT_USERAGENT      => 'Mozilla/5.0 (Windows NT 6.1; rv:8.0) Gecko/20100101 Firefox/8.0', //set user agent
                CURLOPT_RETURNTRANSFER => true,     // return web page
                CURLOPT_HEADER         => false,    // return headers
                CURLOPT_FOLLOWLOCATION => true,     // follow redirects
                CURLOPT_ENCODING       => "",       // handle all encodings
                CURLOPT_AUTOREFERER    => true,     // set referer on redirect
                CURLOPT_CONNECTTIMEOUT => 120,      // timeout on connect
                CURLOPT_TIMEOUT        => 120,      // timeout on response
                CURLOPT_MAXREDIRS      => 10,       // stop after 10 redirects
            );
        }

        $ch = curl_init($url);
        curl_setopt_array($ch,$options);
        $content = curl_exec($ch);
        $err     = curl_errno($ch);
        $errmsg  = curl_error($ch);
        $result  = curl_getinfo($ch);
        curl_close($ch);

        $result['errno']   = $err;
        $result['errmsg']  = $errmsg;
        $result['content'] = $content;
        return $result;
    }

    public static function baseUrl($url){
        if(preg_match(self::$base_url_regex, $url, $matches)) {
            return $matches[0];
        }
        return '';
    }
    public static function getFavicon($content, $url){
        if(preg_match('/rel="shortcut icon".*href="(.*?)"/m', $content, $matches)) {
            $result = $matches[1];
            if(preg_match('/^\/\//', $result)){
                $result = 'http:'.  $result;
            }
            elseif(preg_match('/^\/[^\/]/', $result)){
                $result = self::baseUrl($url). $result;
            }
            if(strpos($result, 'http') === false){
                $result = 'http://'.$result;
            }
            return $result;
        }
        return '';
    }

    public static function getKeywords($content){
        if(preg_match('/name="keywords".*content="(.*?)"/m', $content, $matches)) {
            return $matches[1];
        }
        return '';
    }

    public static function getDescription($content){
        if(preg_match('/name="description".*content="(.*?)"/m', $content, $matches)) {
            return $matches[1];
        }
        return '';
    }
    public static function getTitle($content, $url){
        if(preg_match('/<title>(.*?)<\/title>/m', $content, $matches)) {
            return $matches[1];
        }
        return $url;
    }

}